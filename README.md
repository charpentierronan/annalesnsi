# annalesNSI


Ce dépot contient les pdfs de tous les sujets de NSI donnés au bac depuis 2022,
ainsi que (à terme) les mêmes sujets découpés par exercice, chaque exercice étant accompagné
d'un fichier ipynb pour vérifier ses réponses, d'un corrigé, d'étiquettes, et de commentaires.

Les répertoires correspondent aux années, et les sous répertoires coorespondent à :
* `pdfs` les fichiers pdf scannés contenant les sujets tels qu'ils ont été donnés
* `src` contient les fichiers LaTeX, découpés par exercice, les tableaux, programmes et figures étant séparés avec un système de nommage assez transparent.
* le répertoire lui-même contient les pdfs directement utilisables, et les notebooks,
* le README liste les exercices et les thèmes qui y apparaissent. 
 
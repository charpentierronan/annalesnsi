# Sujets de NSI de 2021

Les sujets donnés en 2021 sont ceux-ci :

* 21_nsizero **Sujet zéro a** 1er mars 2021
  * 21_nsizeroex1 : pile, tri "tri d'une pile de crêpes"
  * 21_nsizeroex2 : grille, récursivité "chemin maximal dans une grille"
  * 21_nsizeroex3 : arbre binaire, ABR "taille et hauteur d'un arbre binaire" 
  * 21_nsizeroex4 : base de données, SQL, csv "élèves de seconde" 
  * 21_nsizeroex5 : routage, RIP, OSPF ""


* 21_nsij1me1 **Métropole J1** 15 mars 2021
  * 21_nsij1me1ex1 : ABR, POO, parcours, recherche "val"
  * 21_nsij1me1ex2 : bash, processus, ordonnancement, interblocage, cryptographie "cryptage xor"
  * 21_nsij1me1ex3 : base de données, SQL "train"
  * 21_nsij1me1ex4 : récursivité, diviser pour régner "tri fusion"
  * 21_nsij1me1ex5 : routage, adressage IP, OSPF ""

** 21_nsij2me1 **Métropole J2** 16 mars 2021
  * 21_nsij2me1ex1 : ABR, POO "biens immobiliers"
  * 21_nsij2me1ex2 : base de données, SQL "réservation en ligne au restaurant"
  * 21_nsij2me1ex3 : réseau, adressage IP, routage ""
  * 21_nsij2me1ex4 : OS, bash, processus "bureau d'architectes"
  * 21_nsij2me1ex5 : piles et files "couple de piles"

** 21_nsij2po1 **Polynésie J2** 16 mars 2021
  * 21_nsij2po1ex1 : tri insertion, tri fusion, "algorithmes de tri"
  * 21_nsij2po1ex2 : base de données, protocoles GET/POST, SQL "plateforme de vente en ligne"
  * 21_nsij2po1ex3 : ABR, POO, insertion dans un ABR "tri par ABR" 
  * 21_nsij2po1ex4 : routage, RIP, interblocage, architecture matérielle, SoC "von Neumann et Harvard"
  * 21_nsij2po1ex5 : données en tables, base de données, SQL "vidéos à la demande"

** 21_nsij1an1 **Amérique du Nord J1** 24 mars 2021
  * 21_nsij1an1ex1 : base de données, SQL "vidéoprojecteurs et imprimantes"
  * 21_nsij1an1ex2 : routage, processus, interblocage, SoC "tracer le réseau à partir des tables de routage"
  * 21_nsij1an1ex3 : tableaux, python "réductions"
  * 21_nsij1an1ex4 : arbres binaires "arbres de compétition"
  * 21_nsij1an1ex5 : piles, files "jaune, rouge, vert"

** 21_nsij1me2 **Métropole J1 juin** 7 juin 2021
  * 21_nsij1me2ex1 : base de données, SQL "Tartuffe"
  * 21_nsij1me2ex2 : pile, POO "fonction mystere"
  * 21_nsij1me2ex3 : processus, routage, rip, ospf ""
  * 21_nsij1me2ex4 : grille, parcours de tableaux "sortir du labyrinthe"
  * 21_nsij1me2ex5 : tableaux, récursivité, diviser pour régner "nombre d'inversions" 

** 21_nsij2me2 **Métropole J2 juin** 8 juin 2021
  * 21_nsij2me2ex1 : base de données, SQL "emprunt de livres au CDI" 
  * 21_nsij2me2ex2 : processus, file, interblocage "verrouillage de fichiers"
  * 21_nsij2me2ex3 : ABR, POO, insertion "ABR bien construit"
  * 21_nsij2me2ex4 : programmation, récursivité "mélange de Fisher-Yates"
  * 21_nsij2me2ex5 : programmation "somme maximale d'une sous-séquence"

** 21_nsij1g11 **Centres étrangers J1** 9 juin 2021
  * 21_nsij1g11ex1 : POO, chiffrement "code de César"
  * 21_nsij1g11ex2 : dict "parc de vélo"
  * 21_nsij1g11ex3 : ABR "parcours récursif"
  * 21_nsij1g11ex4 : adressage IP, conversion en écriture binaire ""
  * 21_nsij1g11ex5 : piles "taille d'une pile"

** 21_nsij2g11 **Centres étrangers J2** 10 juin 2021
  * 21_nsij2g11ex1 : piles, récursivité "mélange de deux piles"
  * 21_nsij2g11ex2 : programmation, grille "labyrinthe"
  * 21_nsij2g11ex3 : conversion decimal binaire, cryptographie "codage XOR"
  * 21_nsij2g11ex4 : base de données, SQL "gestion d'un club de hand"
  * 21_nsij2g11ex5 : python, POO "bandeau à LED"

** 21_nsij1me3 **Métropole J1 septembre** 9 septembre 2021
  * 21_nsij1me3ex1 : protocoles de communication, architecture réseau "deux LAN"
  * 21_nsij1me3ex2 : algo, programmation, récursivité, dichotomie "recherche dichotomique"
  * 21_nsij1me3ex3 : base de données, SQL "AirOne"
  * 21_nsij1me3ex4 : POO, specification "LOCAVACANCES"
  * 21_nsij1me3ex5 : ABR ""

** 21_nsij2me3 **Métropole J2 septembre** 10 septembre 2021
  * 21_nsij2me3ex1 : architecture réseau, routage "entreprise Lambda"
  * 21_nsij2me3ex2 : list, dict "les aventuriers du rail"
  * 21_nsij2me3ex3 : base de données, SQL "Mendeleïev"
  * 21_nsij2me3ex4 : POO, spécification "yaourts"
  * 21_nsij2me3ex5 : csv, spécification "prénoms"

# Sujets de NSI de 2024

Les sujets donnés en 2024 sont ceux-ci :

* 24_nsizeroa **Sujet zéro a** 1er mars 2024
  * 24nsizeroaex1 : architecture, réseaux, routage, rip, ospf "routeurs"
  * 24nsizeroaex2 : python, list, dict "QCM"
  * 24nsizeroaex3 : graphe, bfs, sgbd, sql "cartographie GPS"

* 24_nsizerob **Sujet zéro b** 1er mars 2024
  * 24nsizeroaex1 : list, récursivité, programmation dynamique "score de confiance"
  * 24nsizeroaex2 : os, arborescence des fichiers, processus, pile, file, ordonnancement, interblocage ""
  * 24nsizeroaex3 : python, dict, données en tables, POO, sql "livres de SF"

* 24_nsij1an1 **Amérique du Nord J1** 21 mai 2024
  * 24_nsij1an1ex1 : python, POO, file, ordonnancement, interblocage "tourniquet"
  * 24_nsij1an1ex2 : graphes, grille, dict, DFS "cercle d'amis" 
  * 24_nsij1an1ex3 : python, csv, bibliothèques, SQL "flashcards et boîte de Leitner"

* 24_nsij2an1 **Amérique du Nord J2** 22 mai 2024
  * 24_nsij2an1ex1 : tri, indices, récursivité, complexité, python "tri de Stooge"
  * 24_nsij2an1ex2 : sql, bases de données "pharmacie"
  * 24_nsij2an1ex3 : adresse IP, POO, structures de données, réseaux, architecture, hachage "blockchain"

* 24_nsij1g11 **Centres étrangers 1 J1** 5 juin 2024
  * 24_nsij1g11ex1 : python, programmation dynamique, graphe, réseau "virus et marche aléatoire" 
  * 24_nsij1g11ex2 : réseaux, routage, CIDR, RIP, OSPF 
  * 24_nsij1g11ex3 : python, POO, base de données relationnelles, SQL, tuple, format de date "camping municipal"

* 24_nsij2g11 **Centres étrangers 1 J2** 6 juin 2024
  * 24_nsij2g11ex1 : python, POO, récursivité "déplacements dans une grille rectangulaire" 
  * 24_nsij2g11ex2 : arborescence de fichiers, POO, récursivité, arbres binaires, arbres, parcours préfixe, OS "arborescence non binaire"
  * 24_nsij2g11ex3 : binaire, bases de données relationnelles, SQL "pompiers"

* 24_nsij1ja1 **Asie Pacifique J1** 10 juin 2024
  * 24_nsij1ja1ex1 : python, POO, algorithmique, list, slices "antennes relais"
  * 24_nsij1ja1ex2 : graphes, programmation, pile, grille, ordonnancement dans un DAG "ordre de réalisation de la recette du pain"
  * 24_nsij1ja1ex3 : python, POO, base de données, SQL, argmax dans une liste "badges et personnel"

* 24_nsij2ja1 **Asie Pacifique J2** 11 juin 2024
  * 24_nsij2ja1ex1 : python, list, dict, diviser pour régner "majorité absolue"
  * 24_nsij2ja1ex2 : programmation, POO, file, pile "vérification d'un parenthésage"
  * 24_nsij2ja1ex3 : base de données relationnelle, SQL, python, ordre lexicographique, slices "collection de CD" 

* 24_nsij1po1 **Polynésie française J1** 19 juin 2024
  * 24_nsij1po1ex1 : adressage IP, binaire, routage, RIP, graphes "réseua dans un lycée"
  * 24_nsij1po1ex2 : récursivité, mémoïsation, programmation dynamique "points au rugby"
  * 24_nsij1po1ex3 : dict, base de données, SQL "tour de France"

* 24_nsij2po1 **Polynésie française J2** 20 juin 2024
  * 24_nsij2po1ex1 : pile, file, graphes, BFS, DFS "excursion dans des villes"
  * 24_nsij2po1ex2 : ABR, POO, récursivité, parcours infixe, préfixe, suffixe "arbre partiellement équiibré"
  * 24_nsij2po1ex3 : protocoles réseau, RIP, OSPF, base de données, SQL, algorithmique, Python, string "réseau informatique d'un hôpital, dossier médical d'un patient, sécurité d'un mot de passe"

* 24_nsij1me1 **Métropole J1** 19 juin 2024
  * 24_nsij1meex1 : POO, graphes, pile, file, BFS, calcul de maximum "hyperliens et popularité"
  * 24_nsij1meex2 : bases de données, routage, RIP, OSPF, SQL "croisières en bateau"
  * 24_nsij1meex3 : POO, ABR, récursivité, données en tables, insertion et délétion dans un ABR "course de chiens de traîneau"

* 24_nsij2me1 **Métropole J2** 20 juin 2024
  * 24_nsij2meex1 : bases de données, sql, sécurisation des transactions "collection de CD" 
  * 24_nsij2meex2 : POO, tri, glouton, récursivité, sac à dos "transport de marchandises"
  * 24_nsij2meex3 : POO, graphes, dict, récursivité "domaine skiable"

